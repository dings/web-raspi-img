---
layout: page
title: Raspberry Pi Debian images
date: 2021-08-11 10:26:29 -0500
permalink: /
---

This Web site is built to help you get [Debian
GNU/Linux](https://www.debian.org/) easily running if you have a
computer of the [Raspberry Pi](https://raspberrypi.org/) family.

**TL;DR [Download tested Debian images for your Raspberry Pi](/tested-images)**

Most of the information provided here is initially carried over from
the corresponding pages in the [Debian
Wiki](https://wiki.debian.org/RaspberryPiImages).

- [What is an *image*? Why do I want one anyway?](/what-is-image)
- [What are the installed defaults and configuration settings?](/defaults-and-settings)
- [Daily auto-built images](/daily-images)
- [Tested images](/tested-images)
- [Instructions to flash an image to an SD card](/how-to-image)
- [Some Frequently Asked Questions (FAQs)](/faq)
- [Help! Contact!](/contact)
